# The-Advanced-PowerShell-Scripting-Crash-Course-2019

In this course, I will take you through from the very basics to the most advanced powershell features and cmdlets.

In under 3 hours, we’ll cover a broad range of topics from CMDLETS to Control Flow, String Manipulation, Advanced functions, different operations, conditional logic, loops, parameter splatting, parameter sets, writing event logs, sending an email with powershell, WMI classes, Powershell remoting, Error handling and a bunch of modules like the Active Directory module, Archiving module as well as the AWS module.