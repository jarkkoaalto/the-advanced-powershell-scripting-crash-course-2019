﻿
Read-Host = "Hi there, what city do you live in"

# 

Get-ChildItem

# Set-Location: Go to a specific path
# Same as cd
Set-Location ..

# Set-Location section02

# New-Item: Create a new File
New-Item text1.txt
New-Item text2.txt
New-Item text3.txt

# Move-Item: Move files from one location to another

Move-Item .\text1.txt '.\Dart'

# Copy-Item: Copy files from one location to another

Copy-item '.\Documents and Settings'

# Get-Content: Get data from a file

$data = Get-Content C:\Users\Jarkko\powershell.txt
$data # print data

