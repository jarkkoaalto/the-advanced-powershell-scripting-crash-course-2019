﻿$foo = "Some string"

$foo | Get-Member
$foo.Contains("there")
$foo.Contains("Some")


[string]$name = "Jarkko"
[int]$age = 25

[int]$sentence = Read-Host "What is your age?"

$age = 25
$age | Get-Member
$age.Equals(12)
$age.Equals(25)

$text = "The quick brown fox jump over lazy dog"
Write-Output $text
$text.Replace("The","a")
Write-Output $text