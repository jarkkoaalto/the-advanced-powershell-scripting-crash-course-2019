﻿$foo = "test"
if($foo -eq "test"){
    Write-Host "The condition has been met"
}else{
    Write-Host "The Condition has not been met"
    }

## 

if(-Not (Test-Path -Path C:\NewFolder)){
    Write-Host "Path does not exits. Creating new folder ..."
mkdir C:\NewFolder
}
else
{
    Write-Host "Folder already exists"
    }


## Multible if else comdision
$password = "Enter the Matrix"
If($password -contains "Entry")
{
    Write-Host "Not quite there yet"
}
    Elseif($password -eq "Matrix")
{
    Write-Host "Getting Close"
}
ElseIf($password -eq "Enter the matrix")
{
    Write-Host "You've guessed it right"
}
Else{
    Write-Host "You're not even close"
    }