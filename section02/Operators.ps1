﻿# Arithmetic Operator
1 + 2 
1 - 3
- 2
2 * 3
2 / 6
5 % 10

#Assigment operators
$var = 1
$var += 3

$var -=3
$var *= 3
$var /= 2
$var %= 2

# Comparison operators
2 -eq 2 # Equal to (==)
2 -ne 4 # Not equal to (!=)
5 -gt 2 # Grater thean (>)
5 -ge 5 # Grather-than or equal to (>=)
5 -lt 10 # Less-than
5 -le 5 # Less-than or equal to (<=)

# String comparison operators:
"MyString" -like "String" # Match using the wildcard
"MyString" -notlike "Other*" # Does not match using the wildcars
"MyString" -match '^String$' # Matches a string using regular
"MyString" -notmatch '^Other$' # Does not match a string using re

# Collection comparasion operators:
"abc", "def" -contains "def" 
"abc", "def" -inotcontains "123"
"def" -in "abc", "def"
"123" -notin "abc", "def"

# Redirection operators
cmdlet > file # Send success output to file, overwriting existion content
cmdlet >> file # Send success output to file, appending to exisiting content
cmdlet 2> file # Send error output to file, overwriting existing content
cmdlet 2>> file #Send error output to file, appending to exisiting content


Get-Process > processes.txt
echo "Enter the MAtrix" >> processes.txt

Get-Content C:\fileexits.txt 2>> error.txt

# Concatenating and multiplying strings
"4" + 2 # "42"
4 + "2" # 6
1,2,3 + "Hello" # 1,2,3,Hello

"3" * 2 # 33
3 * "3" # 6

# logical operators
-and # 
-or
-not
! # Locilcal not