﻿$character = "Gogu"
function SuperHero {
    param($character)

switch($character) {
    "Spiderman" {Write-Host "Time to web my way through New York"}
    "Batman" {Write-Host "Gotham is now a safer place."}
    "Gogu" {Write-Host "Kamehameya, I'm a super saiyan"}
    }
}
SuperHero $character


function Hello
{
param (
    [Parameter(Mandatory, Position=0)]
    [String]$name,
    [Parameter(Mandatory, Position=1)]
    [Int]$age
    )
    "Hello $name, you are $age years old."
}

Hello Jack 44 ## Hello Jack, you are 44 years old.

