﻿function sayHi{
param
(
[Parameter(Mandatory=$true)][ValidateSet('Jarkko','Ryan','Michael',IgnoreCase)]$name,
[Parameter(Mandatory=$true)][ValidateRange(18,46)]$age
)
"Hi $name you name is $age"
}
sayHi