﻿$parameters = @{
From = 'To@mydomain.com' # fake, try use your own
To = 'jarkkoaa@gmail.com' ## fake, try use your own
Subject = 'Email from PowerShell'
body = 'Test Message'
BodyAsHTML = $false

Credential = Get-Credential
DeliveryNotificationOption = 'onSuccess'

Encoding = 'UTF8'
Port = '465'
Priority = 'High'
SetpServer = 'smtp.gmail.com'
UseSSL = $True
}

Send-MailMessage @parameters

# Send-MailMessage -From to@example.com -Subject = "HI There!"