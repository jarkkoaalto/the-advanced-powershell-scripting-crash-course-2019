﻿Function Get-ListofApplications
{
    <# 
    .DESCRIPTON
        Get-ListApplication in an advanced function which can be used to get installed application on local or remote computer.
    .PARAMETER ComputeFilePath
        Specified the path to the txt file. This file should contain one or more computers.

    .EXAMPLE
      Get-ListOfApplication -C

}