﻿Function Get-ListOfApplications
{
   <# 
   .Description
        Get-ListOfApplication is an advanced function which can be used to get installed application on local or remote computer.

   . ParameterComputerFilePath
        Specifies the path to the text file. This file should contains one or more computers.
        
   . Example
        C:\PS> Get-ListofApplications -CNPath C:\users\...\Computerlist.txt
        
        This command specifies the path to an that contains several computers. The 'Get-ListofApplications' list installed application from those computer.
   #>

   [CmdletBinding(DefaultParameterSetName="First")]
   Param
   (
    [Parameter(Mandatory=$true, Position=0, ParameterSetName="First")]
    [Alias('CName')][String[]]$ComputerName, 
    [Parameter(Mandatory=$true, Position=0, ParameterSetName="Second")]  ## $username[ParameterSetName="Login], password[ParameterSetName="Loging"], $name[ParameterSetName="Display"]
    [Alias('CNPath')][String]$ComputerFilePath
    )

    If($ComputerName)
    {
        Foreach($CN in $ComputerName) # test computer  connectivity
        {
            $PingResult = Test-Connection -ComputerName $CN -Count 1 -Quiet
        If($PingResult)
        {
            FindInstalledApplicationInfo -ComputerName $CN
            }
            Else
            {
                Write-Warning "Failed to  connect $CN"
            }
         }
     }

     If($ConsoleFilePath){
        $ConsoleFileName = Get-Content -ComputerName $CN -Count 1 -Quiet
        If($PingResult){
            FindInstalledApplicationInfo -ComputerName $CN
            }
        Else{
            Write-Host "Failed to connect to connet to computer $CN"
            }
        }
    }

Function FindInstalledApplicationInfo($ComputerName)
{
    $Objs = @()
    $RegKey ="HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\*" # Contains info all installed components
    $InstalledAppsInfos = Get-ItemProperty -Path $RegKey

    Foreach($item in $InstalledAppsInfos)
    {
        $Obj = [pscustomobject]@{Computer = $ComputerName;
                                        DisplayName = $item.DisplayName;
                                        DisplayVersion = $item.DisplayVersion;
                                        Publisher = $item.Publisher}
        $Objs += $Obj
    }
   $Objs
}                        